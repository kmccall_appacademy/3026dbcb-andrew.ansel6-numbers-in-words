class Fixnum
  def in_words
    nums_to_words = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety",
      100 => "one hundred",
      1000 => "one thousand",
      1000000 => "one million",
      1000000000 => "one billion",
      1000000000000 => "one trillion"
    }

    # array = self.to_s.split("")
    # array.delete("_")
    # a[0].in_words
    # if array.length == 1 || (arr.length == 2 && self < 20)
    if nums_to_words.key?(self)
      nums_to_words[self]
    elsif self < 100
      calculator = self - (self % 10)
      nums_to_words[calculator] + " " + nums_to_words[self % 10]
    elsif self < 1000
      calculator = (self - (self % 100)) / 100
      output = nums_to_words[calculator] + " hundred"
      if nums_to_words.key?(self - (calculator*100))
        output = output + " " + nums_to_words[self - (calculator*100)] if (self - (calculator*100)) != 0
      else
        output = output + " " + nums_to_words[self - (calculator*100) - (self % 10)] if (self - (calculator*100) - (self % 10)) != 0
        output = output + " " + nums_to_words[self % 10] if self % 10 != 0
      end
      output
    elsif self < 1000000
      calculator = (self - (self % 1000)) / 1000
      num = self - calculator*1000
      output = calculator.in_words + " thousand"

      output = output + " " + num.in_words if num.in_words != "zero"
      output
    elsif self < 1000000000
      num = self
      calculator = (self - (self % 1000000)) / 1000000
      num -= calculator*1000000
      output = calculator.in_words + " million"

      calculator = self - (calculator*1000000)
      calculator = (calculator - (calculator % 1000)) / 1000
      output = calculator.in_words + " thousand" if calculator.in_words != "zero"

      output = output + " " + num.in_words if num.in_words != "zero"
      output
    elsif self < 1000000000000
      hundreds = self
      billions = (self - (self % 1000000000)) / 1000000000
      hundreds -= billions*1000000000
      output = billions.in_words + " billion"

      millions = self - (billions*1000000000)
      calculator = (millions - (self % 1000000)) / 1000000
      hundreds = hundreds - calculator*1000000
      output = output + " " + calculator.in_words + " million" if calculator.in_words != "zero"

      thousands = millions - (calculator*1000000)
      calculator = (thousands - (thousands % 1000)) / 1000
      hundreds = hundreds - calculator*1000
      output = output + " " + calculator.in_words + " thousand" if calculator.in_words != "zero"

      output = output + " " + hundreds.in_words if hundreds.in_words != "zero"
      output

    elsif self < 1000000000000000
      hundreds = self
      trillions = (self - (self % 1000000000000)) / 1000000000000
      hundreds -= trillions*1000000000000
      output = trillions.in_words + " trillion"

      billions = self - (trillions*1000000000000)
      calculator = (billions - (self % 1000000000)) / 1000000000
      hundreds = hundreds - calculator*1000000000
      output = output + " " + calculator.in_words + " billion" if calculator.in_words != "zero"


      millions = billions - (calculator*1000000000)
      calculator = (millions - (millions % 1000000)) / 1000000
      hundreds = hundreds - calculator*1000000
      output = output + " " + calculator.in_words + " million" if calculator.in_words != "zero"

      thousands = millions - (calculator*1000000)
      calculator = (thousands - (thousands % 1000)) / 1000
      hundreds = hundreds - calculator*1000
      output = output + " " + calculator.in_words + " thousand" if calculator.in_words != "zero"

      output = output + " " + hundreds.in_words if hundreds.in_words != "zero"
      output
    end #End of conditional statements

  end #End of Method Defintion
end #End of Class Extension
